#!/usr/bin/env bash

set -x                              # Echo on.


python3 -m pip install Django       # Install Django.

python3 manage.py migrate           # Setup Django.
python3 manage.py createsuperuser   # Setup super user.
