#!/usr/bin/env bash

set -x                                    # Echo on.

python3 manage.py runserver 0.0.0.0:8000  # Run Django server.
