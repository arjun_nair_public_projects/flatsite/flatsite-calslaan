from django.urls import path

from . import views

app_name = 'bierlijst'
urlpatterns = [
    path('', views.index, name='index'),
    path('api', views.api, name='api'),
    # ex: /bierlijst/5/drink/
    path('<int:resident_id>/drink/', views.drink, name='drink'),
    path('<int:resident_id>/drink_add/', views.drink_add, name='drink_add'),
    
    path('<int:resident_id>/crate/', views.crate, name='crate'),
    path('<int:resident_id>/crate_add/', views.crate_add, name='crate_add'),
    
    path('<int:resident_id>/personalTransactions/', views.personalTransactions, name='personalTransactions'),
    path('<int:resident_id>/personalTransactions_action/', views.personalTransactions_action, name='personalTransactions_action'),
]
