from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone

import logging
import math

from ..models import Resident, BeerLog, BankStatus

from decimal import Decimal

import json

logger = logging.getLogger('calslaan.bierlijst.general')


# Create your views here.

def index(request):
	resident_list = Resident.objects.filter(active=1).order_by('-beers')
	for r in resident_list:
		r.statiegeld /= 100
		r.crates = int( math.floor(((r.crates * 24) + r.singleBeerBuy - r.beers) / 24) )
	
	bank_balance = BankStatus.objects.get(status_type='balance')
	context = {'resident_list': resident_list, 'bank_balance': (bank_balance.status_value) / 100}
	return render(request, 'bierlijst/index.html', context)

def drink(request, resident_id):
	r = Resident.objects.get(id=resident_id)
	context = {'resident': r}
	return render(request, 'bierlijst/drink.html', context)

def drink_add(request, resident_id):
	r = Resident.objects.get(id=resident_id)
	count_beer = int(request.POST['count'])
	if(count_beer > 0):
		r.beers += count_beer
		bl = r.beerlog_set.create(drink_date=timezone.now(), quantity=count_beer)
		r.save()
	return HttpResponseRedirect(reverse('bierlijst:index'))



def crate(request, resident_id):
	r = Resident.objects.get(id=resident_id)
	context = {'resident': r}
	return render(request, 'bierlijst/crate.html', context)

def crate_add(request, resident_id):
	r = Resident.objects.get(id=resident_id)
	count_crate = 0
	try:
		count_crate = int(request.POST['crates'])
	except:
		pass
	sg_put = 0
	try:
		sg_put = int(count_crate * 390)
	except:
		pass
	sg_rxd = 0
	try:
		sg_rxd = -int(Decimal(request.POST['sg_rxd']) * 390)
	except:
		pass
	count_beer = 0
	try:
		count_beer = int(request.POST['beers_bought'])
	except:
		pass
	if ( (count_crate >= 0) and (sg_put >= 0) and (sg_rxd <= 0) and (count_beer >= 0) ):
		r.crates += count_crate
		r.statiegeld += sg_put
		r.statiegeld += sg_rxd
		r.singleBeerBuy += count_beer
		crl = r.cratelog_set.create(buy_date=timezone.now(), quantity=count_crate)
		sgpl = r.statiegeldlog_set.create(buy_date=timezone.now(), money=sg_put)
		sgrl = r.statiegeldlog_set.create(buy_date=timezone.now(), money=sg_rxd)
		sbbl = r.singlebeerbuylog_set.create(buy_date=timezone.now(), quantity=count_beer)
		r.save()
	return HttpResponseRedirect(reverse('bierlijst:index')) 
