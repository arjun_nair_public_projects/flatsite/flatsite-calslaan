from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone

import logging
import math

from ..models import Resident, BeerLog, BankStatus

import json

logger = logging.getLogger('calslaan.bierlijst.api')

def api(request):
	jdata = {}
	try:
		jdata = json.loads(request.body)
		print("Request string : %s ." % request.body)
	except:
		pass
	
	jresponse = {
		'response' : 'ok',
		'data' : [ 
		]
	}
	
	if('request' in jdata.keys()):
		if(jdata['request'] == 'add'):
			if(
				('id' in jdata.keys())
				and ('add_beers' in jdata.keys())
			):
				resident_id = 0
				beers = 0
				try:
					resident_id = int(jdata['id'])
					beers = int(jdata['add_beers'])
				except:
					pass
				
				if(
					(resident_id > 0)
					and (beers > 0)
				):
					r = 0
					try:
						r = Resident.objects.get(id=resident_id)
					except:
						pass
					if(r):
						r.beers += beers
						bl = r.beerlog_set.create(drink_date=timezone.now(), quantity=beers)
						r.save()
	
	resident_list = Resident.objects.filter(active=1).order_by('-beers')
	for resident in resident_list:
		new_data = {
			'id'       : resident.id,
			'name'     : resident.name,
			'beers'    : resident.beers,
			'status'   : int( math.floor(((resident.crates * 24) + resident.singleBeerBuy - resident.beers) / 24) )
		}
		jresponse['data'].append(new_data)
	
	
	return HttpResponse(json.dumps(jresponse)) 
