from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone

import logging
import math

from ..models import Resident, BeerLog, BankStatus, BankLog

from decimal import Decimal

import json

logger = logging.getLogger('calslaan.bierlijst.banking') 

def personalTransactions(request, resident_id):
	r = Resident.objects.get(id=resident_id)
	bank_balance_field = BankStatus.objects.get(status_type='balance')
	r.statiegeld /= 100
	context = {'resident': r, 'bank_balance': (bank_balance_field.status_value) / 100}
	return render(request, 'bierlijst/personalTransactions.html', context)


def personalTransactions_action(request, resident_id):

	r = Resident.objects.get(id=resident_id)
	bank_balance_field = BankStatus.objects.get(status_type='balance')
	bank_balance = bank_balance_field.status_value
	amount = 0
	try:
		amount = int(float(request.POST['amount']) * 100)
	except:
		pass
	txtype = request.POST['txtype']

	if(amount > 0):
		if(txtype == 'deposit'):
			r.statiegeld += amount
			bank_balance_field.status_value += amount
			sgl = r.statiegeldlog_set.create(buy_date=timezone.now(), money=(amount))
			bl = BankLog(
				resident_user=resident_id,
				user_identifier='mac_detection_not_implemented',
				resident_affected=resident_id,
				transaction_comment='%s : %s deposit %.2f into the bank.' % (r.name, r.name, (amount / 100)),
				bank_balance_change = amount
			)
			bl.save()
		elif(txtype == 'withdraw'):
			print("Balance : %d, amount : %d." % (bank_balance, amount))
			if(bank_balance >= amount):
				r.statiegeld -= amount
				bank_balance_field.status_value -= amount
				sgl = r.statiegeldlog_set.create(buy_date=timezone.now(), money=(-amount))
				bl = BankLog(
					resident_user=resident_id,
					user_identifier='mac_detection_not_implemented',
					resident_affected=resident_id,
					transaction_comment='%s : %s withdrew %.2f from the bank.' % (r.name, r.name, (amount / 100)),
					bank_balance_change = -amount
				)
				bl.save()
		bank_balance_field.save()
		r.save()
		
		return HttpResponseRedirect(reverse('bierlijst:index'))
