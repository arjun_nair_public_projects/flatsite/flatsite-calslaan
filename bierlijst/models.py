from django.db import models

# Create your models here.
class Resident(models.Model):
	name = models.CharField(max_length=200)
	beers = models.IntegerField(default=0)
	crates = models.IntegerField(default=0)
	singleBeerBuy = models.IntegerField(default=0)
	statiegeld = models.IntegerField(default=0)
	active = models.BooleanField(default=True)
	
	def __str__(self):
		ret = self.name
		return ret

class BeerLog(models.Model):
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    drink_date = models.DateTimeField('date drank')
    quantity = models.IntegerField(default=0)

class SingleBeerBuyLog(models.Model):
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    buy_date = models.DateTimeField('date the beer was bought')
    quantity = models.IntegerField(default=0)
    

class CrateLog(models.Model):
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    buy_date = models.DateTimeField('date the crate was bought')
    quantity = models.IntegerField(default=0)


class StatiegeldLog(models.Model):
    resident = models.ForeignKey(Resident, on_delete=models.CASCADE)
    buy_date = models.DateTimeField('Entry date')
    money = models.IntegerField(default=0)
    
    
    
    
    
# Banking model.
class BankStatus(models.Model):
	status_type = models.CharField(max_length=200)
	status_value = models.IntegerField(default=0)


class BankLog(models.Model):
	resident_user = models.IntegerField(default=0)
	user_identifier = models.CharField(default='', max_length=200)
	resident_affected = models.IntegerField(default=0)
	transaction_comment = models.CharField(max_length=500)
	bank_balance_change = models.IntegerField(default=0)
