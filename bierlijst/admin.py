from django.contrib import admin

from .models import Resident

# Register your models here.
class ResidentAdmin(admin.ModelAdmin):
	fields = ('id', 'name', 'beers', 'active')
	readonly_fields = ('id', 'beers')
admin.site.register(Resident, ResidentAdmin)
